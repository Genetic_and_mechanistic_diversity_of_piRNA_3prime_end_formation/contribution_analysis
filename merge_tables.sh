module load R/2.15.3

#presetting of variables 
NAME_W_A=W1118_AubIP
NAME_W_A3=W1118_Ago3IP
NAME_N_A=Nbr_CRISPR_rep2_AubIP
NAME_N_A3=Nbr_CRISPR_rep2_Ago3IP
NAME_Z_A=Zucsh_fabio_AubIP
NAME_Z_A3=Zucsh_fabio_Ago3IP

#Folder of files to keep - use for other analysis/output
rawFOLDER=/groups/brennecke/JS_RH_3end/git_test/contribution_analysis/

#-------------------------------------------------------------------------------------------------------

Rscript /groups/brennecke/JS_RH_3end/git/contribution_analysis/merge_tables.R NAME1=${NAME_W_A} NAME2=${NAME_N_A} NAME3=${NAME_Z_A} NAME4=${NAME_W_A3} NAME5=${NAME_N_A3} NAME6=${NAME_Z_A3} TMP=${rawFOLDER} 
